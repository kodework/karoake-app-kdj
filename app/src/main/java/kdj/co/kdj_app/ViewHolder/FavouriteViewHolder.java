package kdj.co.kdj_app.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kdj.co.kdj_app.R;

/**
 * Created by Vaibhav Barad on 11/30/2017.
 */

public class FavouriteViewHolder extends RecyclerView.ViewHolder {
    public TextView SongName, UserName, TonightsSingerName;
    ImageView imageView;

    public FavouriteViewHolder(View itemView) {
        super(itemView);
        SongName = (TextView) itemView.findViewById(R.id.SongName);
        UserName = (TextView) itemView.findViewById(R.id.UserName);
        TonightsSingerName = (TextView) itemView.findViewById(R.id.TonightsSingerName);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);

        TonightsSingerName.setVisibility(View.GONE);
        UserName.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
    }
}
