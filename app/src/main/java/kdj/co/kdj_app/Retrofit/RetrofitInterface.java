package kdj.co.kdj_app.Retrofit;


import java.util.ArrayList;

import io.reactivex.Observable;
import kdj.co.kdj_app.Model.Events_model;
import kdj.co.kdj_app.Model.LoginModel;
import kdj.co.kdj_app.Model.PendingApprovalModel;
import kdj.co.kdj_app.Model.SongRequestModel;
import kdj.co.kdj_app.Model.Songs_model;
import kdj.co.kdj_app.Model.TonightListModel;
import kdj.co.kdj_app.Model.UpdatePendingModel;
import kdj.co.kdj_app.Model.UserCountModel;
import kdj.co.kdj_app.Model.UserRegistrationModel;
import kdj.co.kdj_app.Model.YourRequestModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;


/**
 * Created by HP PC on 31-08-2017.
 */

public interface RetrofitInterface {

    @POST("/login.php")
    @FormUrlEncoded
    Observable<ArrayList<LoginModel>> login(@Field("pass") String password, @Field("mob") String mobile);

    @POST("/user_song_count.php")
    @FormUrlEncoded
    Observable<ArrayList<UserCountModel>> getSongCounter(
            @Field("status") String status, @Field("user_id") String userID);

    @POST("/tonights_list.php")
    @FormUrlEncoded
    Observable<ArrayList<TonightListModel>> getTonightsList(
            @Field("status") String status, @Field("event_id") String eventID);

    @POST("/user_request.php")
    @FormUrlEncoded
    Observable<ArrayList<YourRequestModel>> getUserRequest(@Field("user_id") String userID, @Field("event_id") String eventID);

    @GET("/get_event_list.php")
    Observable<ArrayList<Events_model>> getEventList();

    @POST("/events_registration.php")
    @FormUrlEncoded
    Observable<ArrayList<Events_model>> eventRegistration(@Field("id") String id, @Field("event") String event, @Field("kdj") String kdj, @Field("date") String date);

    @POST("/new_user.php")
    @FormUrlEncoded
    Observable<ArrayList<UserRegistrationModel>> userRegistration(@Field("user") String userName, @Field("pass") String password, @Field("email") String email, @Field("mob") String mobileNo, @Field("image") String image);


    @GET()
    Observable<ArrayList<Songs_model>> getSearchResult(@Url String url);

    @POST("/update_user.php")
    @FormUrlEncoded
    Observable<ArrayList<Object>> updateUserDetails(/*@Part MultipartBody.Part image, @Part("email") RequestBody email, @Part("mobile") RequestBody mobile, @Part("id") RequestBody id*/
                                                    @Field("image") String image, @Field("email") String email, @Field("mobile") String mobile, @Field("id") String id);

    @POST("/send_request.php")
    @FormUrlEncoded
    Observable<ArrayList<SongRequestModel>> songRequest(@Field("singer_name") String singlerName, @Field("song_name") String songName, @Field("event_id") String eventID, @Field("date") String date,
                                                        @Field("user_id") String userID, @Field("status") String status, @Field("time") String time);

    @POST("/pending_approval.php")
    @FormUrlEncoded
    Observable<ArrayList<PendingApprovalModel>> getPendingApproval(@Field("event_id") String eventID,@Field("status") String status);

    @POST("/update_pending.php")
    @FormUrlEncoded
    Observable<UpdatePendingModel> updatePendingRequest(@Field("id") String id, @Field("status") String status);

    @POST("/delete_event.php")
    @FormUrlEncoded
    Observable<Object> deleteEvent(@Field("id") String id);
}
