package kdj.co.kdj_app.Retrofit;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by HP PC on 31-08-2017.
 */

public class RetrofitApiUtils {

    public static RequestBody JSONToRetrofitJSON(String JSONRequest) throws JSONException {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(JSONRequest)).toString());
    }

    public static RetrofitInterface getGeneralAPIService() {
        return RetrofitClient.getClient("http://kdj.kwrk.in/").create(RetrofitInterface.class);
    }

    /**
     *
     * @param unPreparedObservable pass the {@link RetrofitInterface} function that you want to call and cast the observable to that function
     * @return an observable to attach too, to get the result
     */
    public static Observable<?> getPreparedObservable(Observable<?> unPreparedObservable) {
        Observable<?> preparedObservable = unPreparedObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
            preparedObservable = preparedObservable.cache();
        return preparedObservable;
    }
}
