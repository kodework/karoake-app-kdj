package kdj.co.kdj_app.Activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;

import java.util.ArrayList;

import kdj.co.kdj_app.Adapter.FavouriteListAdapter;
import kdj.co.kdj_app.Model.UserCountModel;
import kdj.co.kdj_app.R;

/**
 * Created by Vaibhav Barad on 11/30/2017.
 */

public class FavouriteListingActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    private ProgressBar spinner;
    ArrayList<UserCountModel> songData;
    Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        spinner = (ProgressBar) findViewById(R.id.progressBar1);

        setupToolbar();

        songData = getIntent().getParcelableArrayListExtra("data");
        if (songData != null && songData.size() > 0) {
            mRecyclerView.setAdapter(new FavouriteListAdapter(FavouriteListingActivity.this, songData));
            mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("Favourite List");
        toolbar.setTitleTextColor(getResources().getColor(R.color.black_overlay));
        Drawable drawable = toolbar.getNavigationIcon();
        drawable.setColorFilter(ContextCompat.getColor(this, R.color.black_overlay), PorterDuff.Mode.SRC_ATOP);
    }

    //default back button
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
