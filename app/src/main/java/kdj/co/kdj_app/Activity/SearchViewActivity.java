package kdj.co.kdj_app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Adapter.SongsListAdapter;
import kdj.co.kdj_app.LandingActivity;
import kdj.co.kdj_app.Model.Songs_model;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;

//import android.support.v7.widget.SearchView;

public class SearchViewActivity extends AppCompatActivity implements /* SearchView.OnQueryTextListener,*/  SongsListAdapter.ItemClickListener/*,SearchView.OnKeyListener */ {
    AutoCompleteTextView searchView;

    RecyclerView mListView;
    String[] mStrings = {"Android ", "java", "IOS", "SQL", "JDBC", "Web services"};
    ArrayList<Songs_model> songModel;
    String KEY_TITLE = "title";
    String KEY_AUTHOR = "artist_name";
    ArrayAdapter adapter;

    RetrofitInterface retrofitInterface;
    private Disposable Subscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_search_view);
        searchView = (AutoCompleteTextView) findViewById(R.id.searchView);
        mListView = (RecyclerView) findViewById(R.id.list_view);
        mListView.setLayoutManager(new LinearLayoutManager(this));
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() >= 2) {
                    sendRequest(charSequence.toString());

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mAdapter != null) {
                    sendRequest(editable.toString());
                    mAdapter.notifyDataSetChanged();
                }
            }

            public boolean onKey(View v, int keyCode, final KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                    sendRequest(event.toString());
                    mAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });
    }

    SongsListAdapter mAdapter;

    private void showJSON(String json) {

    }

    private void sendRequest(String song) {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<Songs_model> request = (Observable<Songs_model>) RetrofitApiUtils.getPreparedObservable(
                retrofitInterface.getSearchResult("http://api.musicgraph.com/api/v2/track/suggest?api_key=c8303e90962e3a5ebd5a1f260a69b138&prefix=" + song + "&limit=5"));
        request.subscribe(new Observer<Songs_model>() {
            @Override
            public void onSubscribe(Disposable d) {
                Subscription = d;
            }

            @Override
            public void onNext(Songs_model songs_models) {
                if (songs_models != null) {
                    mAdapter = new SongsListAdapter(SearchViewActivity.this, songs_models.getData());
                    mListView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.setClickListener(SearchViewActivity.this);

                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }


    public void onItemClick(View view, int position) {
        Log.i("TAG", "You clicked number " + mAdapter.getItem(position) + ", which is at cell position " + position);
        Intent j = new Intent(SearchViewActivity.this, LandingActivity.class);
        String strName = mAdapter.getItem(position).getTitle();
        String strAuthor = mAdapter.getItem(position).getArtistName();
        j.putExtra("name", strName);
        j.putExtra("author", strAuthor);
        startActivity(j);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent j = new Intent(SearchViewActivity.this, LandingActivity.class);
        startActivity(j);
        finish();

    }

}
