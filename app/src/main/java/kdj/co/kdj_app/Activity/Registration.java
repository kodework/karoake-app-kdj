package kdj.co.kdj_app.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.MainActivity;
import kdj.co.kdj_app.Model.UserRegistrationModel;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;

public class Registration extends AppCompatActivity {

    String name1, email1, mob1, pass1;
    EditText name;
    EditText email;
    EditText mob;
    EditText pass;

    TextView join;
    TextView bottom1;
    TextView bottom2;

    Button signup;

    RetrofitInterface retrofitInterface;
    private Disposable subscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Medium.ttf");
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Hairline.ttf");
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "fonts/Lato-LightItalic.ttf");

        name = (EditText) findViewById(R.id.editText2);
        email = (EditText) findViewById(R.id.editText4);
        mob = (EditText) findViewById(R.id.editText5);
        pass = (EditText) findViewById(R.id.editText6);

//         join = (TextView) findViewById(R.id.textView5);
        bottom1 = (TextView) findViewById(R.id.textView9);
        bottom2 = (TextView) findViewById(R.id.textView10);


        signup = (Button) findViewById(R.id.button);
        mob.setTypeface(custom_font2);
        pass.setTypeface(custom_font2);
        email.setTypeface(custom_font2);
        name.setTypeface(custom_font2);
        signup.setTypeface(custom_font);
//            join.setTypeface(custom_font);
        bottom1.setTypeface(custom_font3);


        signup.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                name1 = name.getText().toString();
                email1 = email.getText().toString();
                mob1 = mob.getText().toString();
                pass1 = pass.getText().toString();

                if (!(isEmailValid(email1) && (mob1.length() == 10) && (pass1.length() >= 6))) {
                    if (!(isEmailValid(email1))) {
                        Toast.makeText(Registration.this, "Invalid Email", Toast.LENGTH_LONG).show();
                    }

                    if (!(mob1.length() == 10)) {
                        Toast.makeText(Registration.this, "Please enter a 10 digit mobile number", Toast.LENGTH_LONG).show();
                    }

                    if (!(pass1.length() >= 6)) {
                        Toast.makeText(Registration.this, "Password too short", Toast.LENGTH_LONG).show();
                    }

                } else {
                    if (retrofitInterface == null) {
                        retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
                    }
                    Observable<ArrayList<UserRegistrationModel>> request = (Observable<ArrayList<UserRegistrationModel>>) RetrofitApiUtils.getPreparedObservable(
                            retrofitInterface.userRegistration(name1, pass1, email1, mob1, "http://kdj.kwrk.in/UserPhotos/profilepic.png"));

                    request.subscribe(new Observer<ArrayList<UserRegistrationModel>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            subscription = d;
                        }

                        @Override
                        public void onNext(ArrayList<UserRegistrationModel> userRegistrationModels) {
                            if (userRegistrationModels != null && userRegistrationModels.get(0).getResult() == 1) {
                                SharedPreferences.Editor editor = getSharedPreferences("Login-Info", MODE_PRIVATE).edit();
                                editor.putString("name", userRegistrationModels.get(0).getName());
                                editor.putString("admin", userRegistrationModels.get(0).getAdmin());
                                editor.putString("pass", userRegistrationModels.get(0).getPass());
                                editor.putString("email", userRegistrationModels.get(0).getEmail());
                                editor.putString("mob", userRegistrationModels.get(0).getMob());
                                editor.putString("id", userRegistrationModels.get(0).getId());

                                editor.commit();


                                Toast.makeText(Registration.this, "You are registered as a user", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(Registration.this, MainActivity.class);
                                startActivity(i);
                                finish();

                            } else {
                                Toast.makeText(Registration.this, "Mobile NO. already in use.", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(Registration.this, e.toString(), Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
                }
            }
        });

    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
