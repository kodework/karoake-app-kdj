package kdj.co.kdj_app.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Adapter.MyRecyclerViewAdapter;
import kdj.co.kdj_app.LandingActivity;
import kdj.co.kdj_app.Model.Events_model;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;


public class Events extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {
    public static final String JSON_URL = "http://kdj.kwrk.in/get_event_list.php";
    private Button buttonGet;
    private RecyclerView listView;

    TextView date_text;
    Button b3;
    int dp_month, dp_year = 0, dp_day;
    MyRecyclerViewAdapter adapter;
    TextView event_text;
    EditText name;
    EditText kdj;
    RelativeLayout save;
    TextView grid_box;
    LinearLayout eventgrid;
    RelativeLayout HideForAdmin1, HideForAdmin2, HideForAdmin3;
    SharedPreferences prefs;
    String StrAdmin, AdminStatus;
    SwipeRefreshLayout swipeRefreshLayout;
    Boolean isAdmin;
    public static String EvtID;
    RetrofitInterface retrofitInterface;
    private Disposable Subscription;
    ArrayList<Events_model> mArraModal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        listView = (RecyclerView) findViewById(R.id.rvNumbers);
        event_text = (TextView) findViewById(R.id.textView7);
        name = (EditText) findViewById(R.id.editText9);
        kdj = (EditText) findViewById(R.id.editText7);
        date_text = (TextView) findViewById(R.id.dateField);
        b3 = (Button) findViewById(R.id.button3);
        save = (RelativeLayout) findViewById(R.id.button_save);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshEvents();
                onItemsLoadComplete();
            }
        });

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Bebas-Neue.ttf");
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");

        event_text.setTypeface(custom_font);
        name.setTypeface(custom_font2);
        kdj.setTypeface(custom_font2);
        date_text.setTypeface(custom_font2);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        SharedPreferences AdminPrefs = getSharedPreferences("Login-Info", MODE_PRIVATE);
        StrAdmin = AdminPrefs.getString("admin", AdminStatus);
        isAdmin = StrAdmin.equals("1");
        if (!isAdmin) {
            HideForAdmin1 = (RelativeLayout) findViewById(R.id.CreateEventText);
            HideForAdmin2 = (RelativeLayout) findViewById(R.id.CreateEventBox1);
            HideForAdmin3 = (RelativeLayout) findViewById(R.id.CreateEventBox2);
            HideForAdmin1.setVisibility(LinearLayout.GONE);
            HideForAdmin2.setVisibility(LinearLayout.GONE);
            HideForAdmin3.setVisibility(LinearLayout.GONE);
        }
        //clearPrefs();
        date_text.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(999);
            }
        });


        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(999);
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ((name.getText().toString().length() != 0) && (kdj.getText().toString().length() != 0) && dp_year != 0) {
                    new AlertDialog.Builder(Events.this)
//                            .setTitle("Authentication Failure")
                            .setMessage("Confirm to create this event?")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    api_events_call();
                                    dialog.dismiss();
                                }
                            }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                } else {
                    Toast.makeText(Events.this, "All fields are required", Toast.LENGTH_LONG).show();
                }

            }
        });
        int numberOfColumns = 2;
        listView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        //final String url = "http://kdj.kwrk.in/get_event_list.php";
        sendRequest();
    }

    void refreshEvents() {
        sendRequest();
    }

    void onItemsLoadComplete() {
        swipeRefreshLayout.setRefreshing(false);
    }

    ArrayList<Events_model> aArraMode;

    private void sendRequest() {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }

        //id-timerID;total
        Observable<ArrayList<Events_model>> request = (Observable<ArrayList<Events_model>>) RetrofitApiUtils.getPreparedObservable(retrofitInterface.getEventList());
        request.subscribe(new Observer<ArrayList<Events_model>>() {
            @Override
            public void onSubscribe(Disposable d) {
                Subscription = d;
            }

            @Override
            public void onNext(ArrayList<Events_model> eventModel) {
                if (eventModel != null) {
                    mArraModal = eventModel;
                    adapter = new MyRecyclerViewAdapter(Events.this, mArraModal);
                    adapter.setClickListener(Events.this);
                    listView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    name.setText("");
                    kdj.setText("");

                    date_text.setText("");
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

    }



/* API get data for Event List EOC*/

    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            Calendar c = Calendar.getInstance();


            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int date = c.get(Calendar.DATE);
            DatePickerDialog dialog = new DatePickerDialog(this, myDateListener, year, month, date);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis()- 1000);
            return dialog;

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            Calendar calendar = Calendar.getInstance();
            if ((arg1 >= calendar.get(Calendar.YEAR)) && (arg2 >= calendar.get(Calendar.MONTH)) && (arg3 >= calendar.get(Calendar.DATE))) {
                Log.e("test", "y" + calendar.get(Calendar.YEAR) + " m " + calendar.get(Calendar.MONTH) + " d " + calendar.get(Calendar.DATE));
                dp_year = arg1;
                dp_month = arg2 + 1;
                dp_day = arg3;
                date_text.setText("" + arg3 + "/" + dp_month + "/" + (arg1 - 2000));
            } else {
                Toast.makeText(Events.this, "choose a proper date", Toast.LENGTH_LONG).show();
            }
            //  Toast.makeText(Events.this, " date -"+arg3+ "-"+arg2+"-"+arg1, Toast.LENGTH_LONG).show();
        }
    };


    public void clearPrefs() {
        SharedPreferences.Editor editor = getSharedPreferences("Login-Info", MODE_PRIVATE).edit();
        editor.putString("name", "");
        editor.putString("admin", "");
        editor.putString("pass", "");
        editor.putString("email", "");
        editor.putString("mob", "");
        editor.putString("id", "");
        editor.commit();
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent j = new Intent(Events.this, LandingActivity.class);
        EvtID = mArraModal.get(position).getId();
        startActivity(j);

    }

    public void api_events_call() {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        SharedPreferences prefs = getSharedPreferences("Login-Info", MODE_PRIVATE);
        String mob_id = prefs.getString("id", "");
        Observable<ArrayList<Events_model>> request = (Observable<ArrayList<Events_model>>) RetrofitApiUtils.getPreparedObservable(
                retrofitInterface.eventRegistration(mob_id, name.getText().toString(), kdj.getText().toString(), dp_month + "/" + dp_day + "/" + dp_year));
        request.subscribe(new Observer<ArrayList<Events_model>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Events_model> events_models) {
                if (events_models != null)
                    sendRequest();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

    }
}
