package kdj.co.kdj_app.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Activity.Events;
import kdj.co.kdj_app.Model.SongRequestModel;
import kdj.co.kdj_app.Model.Songs_model;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;

//import android.app.Fragment;

public class RequestSong extends Fragment {

    private AutoCompleteTextView text;
    String[] languages = {"Android ", "java", "IOS", "SQL", "JDBC", "Web services"};
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public String SongName, AristName, eventID, UserID, SongTitle, idTemp;
    EditText Song_Name;
    EditText Artist_Name;
    Button Request_btn;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public static String eventid;
    RetrofitInterface retrofitInterface;
    private Disposable subscription;


    int passEventID;
    private OnFragmentInteractionListener mListener;
    private static String strname, evtID, userID, strartist;
    private static String date;

    public RequestSong() {
    }

    ArrayList<Songs_model> songModel;
    String KEY_TITLE = "title";
    AutoCompleteTextView songView;
    AutoCompleteTextView actv;
    EditText artist, StaticSongName;

    public static RequestSong newInstance(String param1, String param2) {
        RequestSong fragment = new RequestSong();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void send_song_request() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_request_song, container, false);
//        actv = (AutoCompleteTextView) view.findViewById(R.id.searchSongView);
        StaticSongName = (EditText) view.findViewById(R.id.StaticSongView);
        artist = (EditText) view.findViewById(R.id.artist_name);
//        if (getArguments() != null) {
//            strname = getArguments().getString("name");
//            strartist=getArguments().getString("author");
//            eventid =  getArguments().getString("evtName");
//        }


        if (getArguments() != null) {
            userID = getArguments().getString("passUserID");
        }

//COMMENTED SEARCH VIEW ACTIVIY CODE
//        if (strname != null) {
//            actv.setText(strname);
//            artist.setText(strartist);
//        }
//COMMENTED SEARCH VIEW ACTIVIY CODE

        Request_btn = (Button) view.findViewById(R.id.request_btn);
        Song_Name = (EditText) view.findViewById(R.id.StaticSongView);
        Artist_Name = (EditText) view.findViewById(R.id.artist_name);
        String yes;
        Request_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Song_Name.getText().toString().length() != 0) {
                    new AlertDialog.Builder(getActivity())
                            .setMessage("Are you sure you want to send this song request?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    SongSubmit();
                                    dialog.dismiss();
                                }
                            }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                } else {
                    Toast.makeText(getActivity(), "Song name cant be empty", Toast.LENGTH_LONG).show();
                }
            }
        });

        // COMMENTED SEARCH VIEW ACTIVIY CODE
//        actv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    Intent j = new Intent(getActivity(),SearchViewActivity.class);
//                    startActivity(j);
//                    getActivity().finish();
//                }
//            }
//        });
//
//        actv.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent i = new Intent(getActivity(), SearchViewActivity.class);
//                startActivity(i);
//                getActivity().finish();
//            }
//        });
//COMMENTED SEARCH VIEW ACTIVIY CODE
        return view;
    }

    public void SongSubmit() {
        date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        userID = getArguments().getString("passUserID");
        SongName = Song_Name.getText().toString();
        AristName = Artist_Name.getText().toString();
        Date d = new Date();
        CharSequence time = DateFormat.format("hh:mm:ss", d.getTime());

        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }

        Observable<ArrayList<SongRequestModel>> request = (Observable<ArrayList<SongRequestModel>>) RetrofitApiUtils.getPreparedObservable(
                retrofitInterface.songRequest(AristName, SongName, Events.EvtID, date, userID, "", time.toString()));
        request.subscribe(new Observer<ArrayList<SongRequestModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                subscription = d;
            }

            @Override
            public void onNext(ArrayList<SongRequestModel> userRegistrationModels) {
                if (userRegistrationModels != null && userRegistrationModels.get(0).getResult() == 1) {
                    Toast.makeText(getActivity(), "Your Request has been submitted", Toast.LENGTH_LONG).show();
                    Song_Name.setText("");
                    Artist_Name.setText("");

                } else {
                    Toast.makeText(getActivity(), "Please choose a different track: Track not availaible", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
