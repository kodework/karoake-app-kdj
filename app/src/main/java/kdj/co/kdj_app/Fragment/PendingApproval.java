package kdj.co.kdj_app.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Activity.Events;
import kdj.co.kdj_app.Adapter.ApprovalAdapter;
import kdj.co.kdj_app.Model.PendingApprovalModel;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;


public class PendingApproval extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;
    RecyclerView appListView;
    RecyclerView mRecyclerView;
    ArrayList<PendingApprovalModel> pendModel;
    String getjson;
    private ProgressBar spinner;
    private SwipeRefreshLayout swipeView;
    Button AcceptBtn, RejectBtn;
    public static final String REGISTER_SONG_URL = "http://kdj.kwrk.in/pending_approval.php";
    public static String getList;
    RetrofitInterface retrofitInterface;
    private Disposable subscription;

    public PendingApproval() {
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            sendRequest();
        }
    };

    public static PendingApproval newInstance(String param1, String param2) {
        PendingApproval fragment = new PendingApproval();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    ApprovalAdapter mAdapter;

    private void sendRequest() {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<ArrayList<PendingApprovalModel>> request = (Observable<ArrayList<PendingApprovalModel>>) RetrofitApiUtils.getPreparedObservable(
                retrofitInterface.getPendingApproval(Events.EvtID, "0"));
        request.subscribe(new Observer<ArrayList<PendingApprovalModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<PendingApprovalModel> pendingApprovalModels) {
                if (pendingApprovalModels != null) {
                    pendModel = pendingApprovalModels;
                    if (pendModel != null) {
                        mAdapter = new ApprovalAdapter(getActivity(), pendModel);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        spinner.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // appListView= (RecyclerView)

        sendRequest();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_pending_approval, container, false);
        AcceptBtn = (Button) view.findViewById(R.id.accept_btn);
        RejectBtn = (Button) view.findViewById(R.id.reject_btn);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.approval_recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.PendingSwipeRefresh);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
        spinner = (ProgressBar) view.findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);
        sendRequest();
        return view;
    }

    void refreshItems() {
        sendRequest();
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        swipeView.setRefreshing(false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("pending-approval"));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}