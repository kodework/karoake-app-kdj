package kdj.co.kdj_app.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;


import java.util.ArrayList;

import kdj.co.kdj_app.Adapter.TonightListAdapter;
import kdj.co.kdj_app.LandingActivity;
import kdj.co.kdj_app.Model.TonightListModel;
import kdj.co.kdj_app.R;


public class TonightsList extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    public static final String Get_tonight_list = "http://kdj.kwrk.in/tonights_list.php";
    private ArrayList<TonightListModel> pendModel;
    private ProgressBar spinner;
    RecyclerView mRecyclerView;
    SwipeRefreshLayout swipeView;
    private String storejson;
    public static TonightsList newInstance(String param1, String param2) {
        TonightsList fragment = new TonightsList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showJSON();
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tonights_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.tonight_list_recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipetonightslist);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
        spinner = (ProgressBar) view.findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);
        showJSON();
        return view;
    }
    void refreshItems() {
        showJSON();
        onItemsLoadComplete();
    }
    void onItemsLoadComplete() {
        swipeView.setRefreshing(false);
    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    TonightListAdapter mAdapter;
    private void showJSON() {

        pendModel= ((LandingActivity)getActivity()).pendModel;
        if (pendModel!=null) {
            mAdapter= new TonightListAdapter(getActivity(),pendModel);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            spinner.setVisibility(View.GONE);
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }
    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("tonights-list"));
    }
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
