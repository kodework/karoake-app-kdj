package kdj.co.kdj_app.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Activity.FavouriteListingActivity;
import kdj.co.kdj_app.LandingActivity;
import kdj.co.kdj_app.MainActivity;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;
import kdj.co.kdj_app.Utility.CircleTransform;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class UserProfile extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    public String getName, getMob, getEmail, getID;
    private OnFragmentInteractionListener mListener;
    private EditText edit_mob, edit_email;
    private TextView edit_name;
    private ImageView Logout;
    private Bitmap bitmap;
    private int RESULT_LOAD_IMG = 111, getSongInteger;
    private String KEY_IMAGE = "image";
    ProgressDialog progressDialog;
    private ImageView user_image, choose;
    private Button upload;
    private int getSoungCount;
    private String strtext, test;
    private EditText edit_mail, edit_mobile;
    private String imageString, savedImg;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Uri selectedImage;
    TextView Song_count_text, Pixie;
    LinearLayout fav_button;
    RetrofitInterface retrofitInterface;
    private Disposable subscription;

    public UserProfile() {

    }


//    BroadcastReceiver broadcastReceiver= new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            showSongCount();
//        }
//    };
//    BroadcastReceiver updateReceiver= new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            showSongCount();
//        }
//    };
    // private void showSongCount(){
    //getSongInteger=((LandingActivity)getActivity()).SongCount;
    //getSoungCount= ((LandingActivity)getActivity()).SendSongCount;

    //}

    public static UserProfile newInstance(String param1, String param2) {
        UserProfile fragment = new UserProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        prefs = getActivity().getSharedPreferences("store-image", getActivity().MODE_PRIVATE);
        editor = prefs.edit();

    }

    private RoundedBitmapDrawable setCircularImage(int id) {
        Resources res = getActivity().getResources();
        Bitmap src = BitmapFactory.decodeResource(res, id);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(res, src);
        roundedBitmapDrawable.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 2.0f);
        return roundedBitmapDrawable;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        user_image = (ImageView) view.findViewById(R.id.user_image);
        fav_button = (LinearLayout) view.findViewById(R.id.favouriteView);
        //getSongInteger=((LandingActivity)getActivity()).SongCount;
        getSoungCount = ((LandingActivity) getActivity()).SendSongCount;

     /*---------------------Pixie count code----------*/
        Song_count_text = (TextView) view.findViewById(R.id.song_count);
        Pixie = (TextView) view.findViewById(R.id.pixie);

        Song_count_text.setText(String.valueOf(getSoungCount));
        Pixie.setText(String.valueOf(getSoungCount * 50));
        if (getArguments() != null) {
            getName = getArguments().getString("passName");
            getMob = getArguments().getString("passMob");
            getEmail = getArguments().getString("passEmail");
            getID = getArguments().getString("passUserID");
        }
        savedImg = prefs.getString("ImageURI", "");
        //  Toast.makeText(getActivity(),savedImg , Toast.LENGTH_SHORT).show();
        //if(savedImg.equals("")) {
        // Picasso.with(getActivity()).load("http://kdj.kwrk.in/UserPhotos/" + getID + ".png").placeholder(setCircularImage(R.mipmap.profilepic)).error(R.mipmap.profilepic).into(user_image);

        Picasso.with(getActivity()).load("http://kdj.kwrk.in/UserPhotos/" + getID + ".png")
                .placeholder(setCircularImage(R.mipmap.profilepic)).transform(new CircleTransform()).into(user_image);

        // }
        //else {
        //    Picasso.with(getActivity()).load(savedImg).into(user_image);
        //  }
        Logout = (ImageView) view.findViewById(R.id.logout);
        edit_name = (TextView) view.findViewById(R.id.Name);
        edit_mobile = (EditText) view.findViewById(R.id.EditMobile);
        edit_email = (EditText) view.findViewById(R.id.EditMail);
        edit_name.setText(LandingActivity.str_name);
        edit_mobile.setText(LandingActivity.str_mobile);
        edit_email.setText(LandingActivity.str_email);

        String yes;
        Logout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Dou you want to Logout?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(getActivity(), MainActivity.class);
                                startActivity(i);
                                SharedPreferences preferences = getActivity().getSharedPreferences("Login-Info", getActivity().MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.commit();
                                getActivity().finish();
                                dialog.dismiss();
                            }
                        }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();
            }
        });
        int test = LandingActivity.SaveSongCount;
        strtext = getArguments().getString("passUserID");
        choose = (ImageView) view.findViewById(R.id.Choose);
        upload = (Button) view.findViewById(R.id.upload_btn);
        choose = (ImageView) view.findViewById(R.id.Choose);
        edit_mail = (EditText) view.findViewById(R.id.EditMail);
        edit_mob = (EditText) view.findViewById(R.id.EditMobile);
        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            }
        });
//        choose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
//            }
//        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Do you want to Save changes?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                sendUserDetails();
                                dialog.dismiss();
                            }
                        }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();

            }
        });

        fav_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((LandingActivity) getActivity()).count_model.size() > 0) {
                    Intent intent = new Intent(getActivity(), FavouriteListingActivity.class);
                    intent.putParcelableArrayListExtra("data", ((LandingActivity) getActivity()).count_model);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "There are no songs in your favourite list", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    public void sendUserDetails() {
        File file = new File(getActivity().getCacheDir(), "profileImage.jpg");
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Uploading, please wait...");
        progressDialog.show();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            byte[] imageBytes = baos.toByteArray();
            imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }
        //sending image to server
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }

        Observable<ArrayList<Object>> request = (Observable<ArrayList<Object>>) RetrofitApiUtils.getPreparedObservable(
                retrofitInterface.updateUserDetails(imageString, edit_mail.getText().toString(), edit_mobile.getText().toString(), getID));
        request.subscribe(new Observer<ArrayList<Object>>() {
            @Override
            public void onSubscribe(Disposable d) {
                subscription = d;
            }

            @Override
            public void onNext(ArrayList<Object> userRegistrationModels) {
                progressDialog.dismiss();
                editor.putString("ImageURI", String.valueOf(selectedImage));
                editor.commit();
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(getActivity(), "Some error occurred -> " + e.toString(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 111:
                    selectedImage = data.getData();
                    PicassoTools.clearCache(Picasso.with(getActivity()));
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                        //user_image.setImageBitmap(bitmap);
                        Picasso.with(getActivity()).load(selectedImage).transform(new CircleTransform()).into(user_image);

                    } catch (IOException e) {
                        Log.i("TAG", "Some exception " + e);
                    }
                    break;
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        rxUnSubscribe();
    }

    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isDisposed())
            subscription.dispose();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (selectedImage == null) {
            Picasso.with(getActivity()).load("http://kdj.kwrk.in/UserPhotos/" + getID + ".png").placeholder(setCircularImage(R.mipmap.profilepic)).transform(new CircleTransform()).into(user_image);
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
