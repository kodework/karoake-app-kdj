package kdj.co.kdj_app.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import kdj.co.kdj_app.Adapter.YourRequestAdapter;
import kdj.co.kdj_app.LandingActivity;
import kdj.co.kdj_app.Model.YourRequestModel;
import kdj.co.kdj_app.R;


public class YourRequest extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList<YourRequestModel> YreqModel;
    private OnFragmentInteractionListener mListener;
    RecyclerView mRecyclerView;
    private ProgressBar spinner;
    YourRequestAdapter mAdapter;
    private ImageView status_img;
    public static final String GetUserRequests = "http://kdj.kwrk.in/tonights_list.php";
    private SwipeRefreshLayout swipeView;
    BroadcastReceiver broadcastReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showJSON();
        }
    };

    BroadcastReceiver updateReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String deletedID = intent.getStringExtra("id");
            if(YreqModel!=null){
                for (YourRequestModel model:YreqModel) {
                    if(model.getId().equals(deletedID)){
                        model.setStatus(intent.getStringExtra("status"));
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    public void YourRequests() {

    }



    // TODO: Rename and change types and number of parameters
    public static YourRequest newInstance(String param1, String param2) {
        YourRequest fragment = new YourRequest();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_your_request, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.your_request_recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        //status_img=(ImageView) getActivity().findViewById(R.id.statusImg);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });
        showJSON();
        return view;
    }
    void refreshItems() {
        onItemsLoadComplete();
    }
    void onItemsLoadComplete() {
        showJSON();
        mAdapter.notifyDataSetChanged();
        swipeView.setRefreshing(false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    private void showJSON() {
        YreqModel= ((LandingActivity)getActivity()).YreqModel;
        if (YreqModel!=null) {
            mAdapter= new YourRequestAdapter(getActivity(),YreqModel);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(updateReceiver);

    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver,new IntentFilter("your-request"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(updateReceiver,new IntentFilter("request-updated"));

    }
}
