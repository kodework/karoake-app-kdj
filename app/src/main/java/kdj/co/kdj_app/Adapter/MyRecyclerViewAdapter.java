package kdj.co.kdj_app.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Model.Events_model;
import kdj.co.kdj_app.Model.UserRegistrationModel;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;

/**
 * Created by user on 22/05/2017.
 */

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private String[] mData = new String[0];
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    ArrayList<Events_model> eList = new ArrayList<Events_model>();
    Context mContext;
    Events_model EventID;
    RetrofitInterface retrofitInterface;
    private Disposable subscription;    // data is passed into the constructor

    public MyRecyclerViewAdapter(Context activity, ArrayList<Events_model> data) {
        this.mContext = activity;
        this.mInflater = LayoutInflater.from(mContext);
        this.eList = data;

    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //String animal = mData[position];
        holder.myTextView.setText(eList.get(position).getEvent());
        holder.txtDate.setText(eList.get(position).getDate());
        holder.txtkdj.setText(eList.get(position).getKdj());

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return eList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView myTextView, txtDate, txtkdj;
        public ImageView deletevent;

        public ViewHolder(View itemView) {
            super(itemView);
            myTextView = (TextView) itemView.findViewById(R.id.textPlace);
            txtDate = (TextView) itemView.findViewById(R.id.textDate);
            txtkdj = (TextView) itemView.findViewById(R.id.textDj);

//            deletevent=(ImageView) itemView.findViewById(R.id.delete_event);
//            deletevent.setOnClickListener(this);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }


        @Override
        public boolean onLongClick(final View view) {
            new AlertDialog.Builder(mContext)
//                            .setTitle("Authentication Failure")
                    .setMessage("Do you want to delete this event?")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int position) {
                            EventID = eList.get(getLayoutPosition());
                            eList.remove(getLayoutPosition());
                            deleteEvent(EventID.getId());
                            notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setIcon(android.R.drawable.ic_dialog_alert).show();
            return true;
        }
    }

    public void deleteEvent(final String eventID) {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<Object> request = (Observable<Object>) RetrofitApiUtils.getPreparedObservable(
                retrofitInterface.deleteEvent(eventID));
        request.subscribe(new Observer<Object>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Object o) {
                Toast.makeText(mContext, "Deleted ", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onError(Throwable e) {

                Toast.makeText(mContext, "Some error occurred -> " + e.toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onComplete() {

            }
        });
    }


    // convenience method for getting data at click position
    public Events_model getItem(int id) {
        return eList.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
