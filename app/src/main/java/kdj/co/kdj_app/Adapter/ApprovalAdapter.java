package kdj.co.kdj_app.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Model.PendingApprovalModel;
import kdj.co.kdj_app.Model.UpdatePendingModel;
import kdj.co.kdj_app.Model.UserRegistrationModel;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;


public class ApprovalAdapter extends RecyclerView.Adapter<ApprovalAdapter.ViewHolder> {
    private String[] mDataset;
    private LayoutInflater mInflater;
    ArrayList<PendingApprovalModel> pmodel;
    ApprovalAdapter ApdaterInstance;
    Context mContext;
    String storeSongID, storeStatus, Response;
    private ItemClickListener mClickListener;
    RetrofitInterface retrofitInterface;
    private Disposable subscription;

    public ApprovalAdapter(Context activity, ArrayList<PendingApprovalModel> pendModel) {
        this.mContext = activity;
        //this.mInflater = LayoutInflater.from(mContext);
        this.pmodel = pendModel;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //  holder.


        holder.textSongName.setText(pmodel.get(position).getSongName());
        holder.textName.setText(pmodel.get(position).getName());
        holder.textSingerName.setText(pmodel.get(position).getSingerName());
        holder.AcceptBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                storeSongID = pmodel.get(position).getId();
                storeStatus = pmodel.get(position).getStatus();
                new AlertDialog.Builder(mContext)
                        .setMessage("Accept this user request?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SendRequestStatus(storeSongID, 1);
                                notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();
                notifyDataSetChanged();
            }
        });
        holder.RejectBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                storeSongID = pmodel.get(position).getId();
                storeStatus = pmodel.get(position).getStatus();
                new AlertDialog.Builder(mContext)
                        .setMessage("Reject this user request?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SendRequestStatus(storeSongID, 2);
                                notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();
                notifyDataSetChanged();
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/ {
        // each data item is just a string in this case
        public TextView textName, textSongName, textSingerName;
        public Button AcceptBtn, RejectBtn;

        public ViewHolder(View v) {
            super(v);
            textSongName = (TextView) v.findViewById(R.id.songtext);
            textName = (TextView) v.findViewById(R.id.nameText);
            textSingerName = (TextView) v.findViewById(R.id.Singer_Name);
            AcceptBtn = (Button) v.findViewById(R.id.accept_btn);
            RejectBtn = (Button) v.findViewById(R.id.reject_btn);

        }
    }

    private void SendRequestStatus(final String storeSongID, final int i) {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<UpdatePendingModel> request = (Observable<UpdatePendingModel>) RetrofitApiUtils.getPreparedObservable(
                retrofitInterface.updatePendingRequest(storeSongID, String.valueOf(i)));
        request.subscribe(new Observer<UpdatePendingModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(UpdatePendingModel dataModel) {
                if (dataModel != null) {
                    for (int j = 0; j < pmodel.size(); j++) {
                        PendingApprovalModel model = pmodel.get(j);
                        if (model.getId().equals(storeSongID)) {
                            pmodel.remove(j);
                            Intent intent = new Intent("request-updated").putExtra("id", storeSongID).putExtra("status", "" + i);
                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                        }
                    }
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }


    public ApprovalAdapter() {
        //   mDataset = myDataset;
    }

    @Override
    public ApprovalAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.approval_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    public PendingApprovalModel getItem(int id) {
        return pmodel.get(id);
    }

    @Override
    public int getItemCount() {
        return pmodel.size();
    }

    public void setClickListener(ApprovalAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}