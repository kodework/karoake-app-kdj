package kdj.co.kdj_app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kdj.co.kdj_app.Model.TonightListModel;
import kdj.co.kdj_app.R;

/**
 * Created by asd on 19/05/2017.
 */

public class TonightListAdapter extends RecyclerView.Adapter<TonightListAdapter.ViewHolder> {
    //private final Context mContext;
    private String[] mDataset;
    Context mContext;
    ArrayList<TonightListModel> tModel;

    private LayoutInflater mInflater;
    String getSongID, getSongStatus;

    public TonightListAdapter(Context activity, ArrayList<TonightListModel> pendModel) {
        this.mContext=activity;
        this.mInflater = LayoutInflater.from(mContext);
        this.tModel=pendModel;
    }



    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView SongName,UserName,TonightsSingerName;
        public ViewHolder(View  v) {

            super(v);
            SongName= (TextView) v.findViewById(R.id.SongName);
            UserName = (TextView) v.findViewById(R.id.UserName);
            TonightsSingerName=(TextView) v.findViewById(R.id.TonightsSingerName);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)

    // Create new views (invoked by the layout manager)
    @Override
    public TonightListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tonightlist_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.SongName.setText(tModel.get(position).getSongName());
        holder.UserName.setText(tModel.get(position).getName());
        holder.TonightsSingerName.setText(tModel.get(position).getSingerName());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return tModel.size();
    }
}

