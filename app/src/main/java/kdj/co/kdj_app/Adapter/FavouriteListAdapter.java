package kdj.co.kdj_app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kdj.co.kdj_app.Model.UserCountModel;
import kdj.co.kdj_app.R;
import kdj.co.kdj_app.ViewHolder.FavouriteViewHolder;

/**
 * Created by Vaibhav Barad on 11/30/2017.
 */

public class FavouriteListAdapter extends RecyclerView.Adapter {
    Context mContext;
    ArrayList<UserCountModel> songData;

    public FavouriteListAdapter(Context context, ArrayList<UserCountModel> songData) {
        this.mContext = context;
        this.songData = songData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tonightlist_item, parent, false);
        return new FavouriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FavouriteViewHolder) {
            ((FavouriteViewHolder) holder).SongName.setText(songData.get(position).getSongName());
        }
    }

    @Override
    public int getItemCount() {
        return songData.size() > 10 ? 10 : songData.size();
    }
}
