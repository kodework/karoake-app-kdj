package kdj.co.kdj_app.Adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kdj.co.kdj_app.Model.YourRequestModel;
import kdj.co.kdj_app.R;


public class YourRequestAdapter extends RecyclerView.Adapter<YourRequestAdapter.ViewHolder> {
    private String[] mDataset;
    Context mContext;
    ArrayList<YourRequestModel> yModel;
    public String UserSongHistory;
    private LayoutInflater mInflater;
    private MyRecyclerViewAdapter.ItemClickListener mClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public TextView SongName,SongAuthor;
        public ImageView statusImg;
        public ViewHolder(View  v) {
            super(v);
                SongName=(TextView) v.findViewById(R.id.ReqSongName);
                SongAuthor=(TextView) v.findViewById(R.id.ReqSongAuthor);
                statusImg=(ImageView) v.findViewById(R.id.statusImg);
                v.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {

        }
    }

    BroadcastReceiver broadcastReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
                notifyDataSetChanged();
        }
    };


    public YourRequestAdapter(Context activity, ArrayList<YourRequestModel> YreqModel ) {
        this.mContext=activity;
        this.mInflater = LayoutInflater.from(mContext);
        this.yModel=YreqModel;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public YourRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.yourequest_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    String test;
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.SongName.setText(yModel.get(position).getSongName());
        holder.SongAuthor.setText(yModel.get(position).getSingerName());
        if(yModel.get(position).getStatus().equals("1")){
            holder.statusImg.setImageResource(R.mipmap.right);
        }
        else if(yModel.get(position).getStatus().equals("0")){
            holder.statusImg.setImageResource(R.mipmap.reload);
        }
        else if(yModel.get(position).getStatus().equals("2")){
            holder.statusImg.setImageResource(R.mipmap.wrong);
        }

    }
    @Override
    public int getItemCount() {
        return yModel.size();
    }


}