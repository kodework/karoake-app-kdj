package kdj.co.kdj_app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import kdj.co.kdj_app.R;

/**
 * Created by asd on 08/08/2017.
 */

public class UserProfileAdapter extends RecyclerView.Adapter<UserProfileAdapter.ViewHolder> {
    private String[] mDataset;
    private LayoutInflater mInflater;
    Context mContext;
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public EditText edit_mobile,edit_email;
        public TextView edit_name;
        public ViewHolder(View  v) {
            super(v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
    public UserProfileAdapter(Context activity) {
        this.mContext=activity;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public UserProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_user_profile, parent, false);

        UserProfileAdapter.ViewHolder vh = new UserProfileAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(UserProfileAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 33;
    }
}
