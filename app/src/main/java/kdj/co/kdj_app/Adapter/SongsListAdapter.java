package kdj.co.kdj_app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kdj.co.kdj_app.Model.Songs_model;
import kdj.co.kdj_app.R;

public class SongsListAdapter extends RecyclerView.Adapter<SongsListAdapter.ViewHolder> {
    //private String[] mData = new String[0];
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    ArrayList<Songs_model.Datum> eList =  new ArrayList<>();
    // data is passed into the constructor

    public SongsListAdapter(Context context, ArrayList<Songs_model.Datum> data) {
        this.mInflater = LayoutInflater.from(context);
        this.eList = data;
    }

    // inflates the cell layout from xml when needed
    @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.songsearchitem, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //String animal = mData[position];
        holder.myTextView.setText(eList.get(position).getTitle());
       // holder.txtDate.setText(eList.get(position).getDate());
       // holder.txtkdj.setText(eList.get(position).getKdj());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return eList.size();
    }

//    public void setClickListener(SearchViewActivity searchViewActivity) {
//        this.mClickListener = searchViewActivity;
//    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView myTextView,txtDate, txtkdj;

        public ViewHolder(View itemView) {
            super(itemView);
            myTextView = (TextView) itemView.findViewById(R.id.textView8);
          //  txtDate = (TextView) itemView.findViewById(R.id.textDate);
          //  txtkdj = (TextView) itemView.findViewById(R.id.textDj);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Songs_model.Datum getItem(int id) {
        return eList.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

