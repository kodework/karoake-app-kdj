package kdj.co.kdj_app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import kdj.co.kdj_app.R;

/**
 * Created by asd on 27/11/2017.
 */

public class FavAdapter extends ArrayAdapter {
    private Context context;
    private String[] months;
    private TextView favName;

    public FavAdapter(Context context, String[] months) {
        super(context, R.layout.fav_list_item);
        this.context = context;
        this.months = months;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.fav_list_item, null);
            favName = (TextView) convertView.findViewById(R.id.FavItemText);
            convertView.setTag(favName);
        } else {
            favName = (TextView) convertView.getTag();
        }
        favName.setText(months[position]);
        return convertView;
    }
    @Override
    public int getCount() {
        return months.length;
    }

}