package kdj.co.kdj_app.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import kdj.co.kdj_app.Model.Songs_model;


public class AutoCompleteTextViewAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    ArrayList<Songs_model>  eList =  new ArrayList<Songs_model>();

//
//    public MyRecyclerViewAdapter(Context context, ArrayList<Events_model> data) {
//        this.mInflater = LayoutInflater.from(context);
//        this.eList = data;
//    }

    public AutoCompleteTextViewAdapter(FragmentManager manager) {
        super(manager);
    }
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

}