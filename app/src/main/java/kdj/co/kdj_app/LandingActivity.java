package kdj.co.kdj_app;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Activity.Events;
import kdj.co.kdj_app.Adapter.ViewPagerAdapter;
import kdj.co.kdj_app.Fragment.PendingApproval;
import kdj.co.kdj_app.Fragment.RequestSong;
import kdj.co.kdj_app.Fragment.TonightsList;
import kdj.co.kdj_app.Fragment.UserProfile;
import kdj.co.kdj_app.Fragment.YourRequest;
import kdj.co.kdj_app.Model.TonightListModel;
import kdj.co.kdj_app.Model.UserCountModel;
import kdj.co.kdj_app.Model.YourRequestModel;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;


public class LandingActivity extends AppCompatActivity {

    public Toolbar toolbar;
    TabLayout tabL;

    private ViewPager viewPager;
    private ImageView testimg;
    public String get_user_id, str, get_email, get_mobile, get_user_name, get_admin;
    public static String str_name, str_mobile, str_email, str_admin;
    private Bundle bundle, userBundle;
    public static int SaveSongCount;

    public ArrayList<TonightListModel> pendModel;
    public ArrayList<YourRequestModel> YreqModel;
    TonightsList tonightfrg;
    public static ImageView test;
    public static int SendSongCount;
    public static int SongCount;

    boolean isAdmin;

    public static ArrayList<UserCountModel> count_model;

    RetrofitInterface retrofitInterface;
    private Disposable counterSubscription, tonightListDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        SharedPreferences mPrefs = getSharedPreferences("Login-Info", MODE_PRIVATE);
        str = mPrefs.getString("id", get_user_id);
        str_name = mPrefs.getString("name", get_user_name);
        str_mobile = mPrefs.getString("mob", get_mobile);
        str_email = mPrefs.getString("email", get_email);
        str_admin = mPrefs.getString("admin", get_admin);
        count_model = new ArrayList<>();
        getSongCountrequest();
        setSupportActionBar(toolbar);
        bundle = getIntent().getExtras();
        test = (ImageView) findViewById(R.id.statusImg);
//        if (bundle != null) {
//            strName = bundle.getString("name");
//            strArtist=bundle.getString("author");
////            bundle.putString("passUserID", str);
//        }
        mPrefs.getString("id", "");
        userBundle = new Bundle();
        userBundle.putString("passName", str_name);
        userBundle.putString("passMob", str_mobile);
        userBundle.putString("passEmail", str_email);
        userBundle.putString("passUserID", str);
        tabL = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabL.setupWithViewPager(viewPager);
        testimg = (ImageView) findViewById(R.id.imageView3);
        setIndicator();
        testimg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                broadcastIntent(v);
            }
        });
        SaveSongCount = 0;


    }

    ViewPagerAdapter adapter;

    /*----------------------------USer song count API-------------------*/
    private void getSongCountrequest() {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<ArrayList<UserCountModel>> request = (Observable<ArrayList<UserCountModel>>) RetrofitApiUtils.getPreparedObservable(retrofitInterface.getSongCounter("" + 1, str));
        request.subscribe(new Observer<ArrayList<UserCountModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                counterSubscription = d;
            }

            @Override
            public void onNext(ArrayList<UserCountModel> loginModels) {
                if (loginModels != null) {
                    count_model = loginModels;
                    SongCount = count_model.size();
                    SendSongCount = SongCount;
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    /*=====================TONIGHTS LIST API CALL=====================*/
    private void sendRequest() {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<ArrayList<TonightListModel>> request = (Observable<ArrayList<TonightListModel>>) RetrofitApiUtils.getPreparedObservable(retrofitInterface.getTonightsList("" + 1, Events.EvtID));
        request.subscribe(new Observer<ArrayList<TonightListModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                tonightListDisposable = d;
            }

            @Override
            public void onNext(ArrayList<TonightListModel> tonightListModels) {
                if (tonightListModels != null) {
                    pendModel = tonightListModels;
                    Intent intent = new Intent("tonights-list");
                    LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(intent);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }


    public void broadcastIntent(View view) {
        Intent intent = new Intent(LandingActivity.this, RequestSong.class);

        intent.setAction("com.tutorialspoint.CUSTOM_INTENT");
        sendBroadcast(intent);
    }

    private void sendUserSongRequest() {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        final ArrayList<String> iconList = new ArrayList<String>();
        Observable<ArrayList<YourRequestModel>> request = (Observable<ArrayList<YourRequestModel>>) RetrofitApiUtils.getPreparedObservable(retrofitInterface.getUserRequest(str, Events.EvtID));
        request.subscribe(new Observer<ArrayList<YourRequestModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<YourRequestModel> userRequestModel) {
                if (userRequestModel != null) {
                    YreqModel = userRequestModel;
                    for (YourRequestModel mEModal : userRequestModel) {
                        if (mEModal.getUserId().equals(str)) {
                            iconList.add(mEModal.getStatus());
                        }
                    }
                    SaveSongCount = iconList.size();
                    Intent intent = new Intent("your-request");
                    LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(intent);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        RequestSong addfrag = new RequestSong();
        PendingApproval pendingfrag = new PendingApproval();
        UserProfile userprofilefrag = new UserProfile();
        if (userBundle != null) {
            addfrag.setArguments(userBundle);
        }
        userprofilefrag.setArguments(userBundle);
        YourRequest yourfrag = new YourRequest();
        tonightfrg = new TonightsList();
        isAdmin = str_admin.equals("1");
        if (isAdmin) {
            adapter.addFragment(pendingfrag);
            adapter.addFragment(tonightfrg);
            adapter.addFragment(userprofilefrag);
        } else {
            adapter.addFragment(addfrag);
            adapter.addFragment(yourfrag);
            adapter.addFragment(tonightfrg);
            adapter.addFragment(userprofilefrag);
        }
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (isAdmin) {
                    if (position == 0) {
                        //getSongCountrequest();
                        Intent intent = new Intent("pending-approval");
                        LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(intent);
                        Intent i = new Intent("request-adapter");
                        LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(i);
                    }
                    if (position == 1) {
                        // if (pendModel == null) {
                        //  sendUserSongRequest();
                        sendRequest();
                        // getSongCountrequest();
                        // }
                        // Toast.makeText(LandingActivity.this, "reached 1", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (position == 0) {
                        //getSongCountrequest();
                    }
                    if (position == 1) {

                        // if (YreqModel == null) {

                        // }
                        sendUserSongRequest();
                        //if (pendModel == null) {
                        //getSongCountrequest();
                        sendRequest();
                        // }
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (isAdmin) {
                    if (position == 0) {
//                        if (YreqModel == null) {
//                            sendUserSongRequest();
//                        }
                        //getSongCountrequest();
                        Intent intent = new Intent("pending-approval");
                        LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(intent);
                        Intent i = new Intent("request-adapter");
                        LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(i);

                    }
                    if (position == 1) {
                        //if (pendModel == null) {
                        // sendUserSongRequest();
                        //getSongCountrequest();
                        sendRequest();
                        //}
                    }
                } else {
                    if (position == 0) {
                        //getSongCountrequest();

                    }
                    if (position == 1) {
                        //if (YreqModel == null) {
                        //getSongCountrequest();
                        sendUserSongRequest();
                        //}
                        //if (pendModel == null) {
                        sendRequest();

                        //}
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int position) {
//                if (isAdmin) {
//                    if (position == 0) {
////                        if (YreqModel == null) {
////                            sendUserSongRequest();
////                        }
//                        Intent intent = new Intent("pending-approval");
//                        LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(intent);
//                        Intent i = new Intent("request-adapter");
//                        LocalBroadcastManager.getInstance(LandingActivity.this).sendBroadcast(i);
//                    }
//                    if (position == 1) {
//                        if (pendModel == null) {
//                            sendRequest();
//                        }
//                    }
//                }
//                else {
//                    if (position == 1) {
//                        if (YreqModel == null) {
//                            sendUserSongRequest();
//                        }
//                        if (pendModel == null) {
//                            sendRequest();
//                        }
//                    }
//                }
            }
        });
    }

    public void setIndicator() {
        //getSongCountrequest();
        for (int i = 0; i < adapter.getCount(); i++) {
            if (isAdmin) {
                switch (i) {
                    case 0:
                        tabL.getTabAt(i).setIcon(R.mipmap.music);
//                    toolbar.setTitle("Song Request");
                        break;
                    case 1:
                        tabL.getTabAt(i).setIcon(R.mipmap.whitelist);
                        // toolbar.setTitle("Pending Approval");
                        break;
                    case 2:
                        tabL.getTabAt(i).setIcon(R.mipmap.whitecog);
                        //toolbar.setTitle("Your Request");
                        break;
                    default:
                        break;
                }
            } else {
                switch (i) {
                    case 0:
                        tabL.getTabAt(i).setIcon(R.mipmap.plus);
//                    toolbar.setTitle("Song Request");
                        break;
                    case 1:
                        tabL.getTabAt(i).setIcon(R.mipmap.whitesong_schedule);
                        // toolbar.setTitle("Pending Approval");
                        break;
                    case 2:
                        tabL.getTabAt(i).setIcon(R.mipmap.whitelist);
                        //toolbar.setTitle("Your Request");
                        break;
                    case 3:
                        tabL.getTabAt(i).setIcon(R.mipmap.whitecog);
                        //toolbar.setTitle("Tonight's List");
                        break;
                    default:
                        break;
                }
            }
        }

        tabL.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (isAdmin) {
                    switch (tabL.getSelectedTabPosition()) {

                        case 0:
                            tabL.getTabAt(0).setIcon(R.mipmap.music);
                            tabL.getTabAt(1).setIcon(R.mipmap.whitelist);
                            tabL.getTabAt(2).setIcon(R.mipmap.whitecog);
                            // toolbar.setTitle("Song Request");
                            //do what you want when tab 0 is selected
                            break;
                        case 1:
                            tabL.getTabAt(0).setIcon(R.mipmap.whitemusic);
                            tabL.getTabAt(1).setIcon(R.mipmap.list);
                            tabL.getTabAt(2).setIcon(R.mipmap.whitecog);
                            //toolbar.setTitle("Pending Approval");
                            break;
                        case 2:
                            tabL.getTabAt(0).setIcon(R.mipmap.whitemusic);
                            tabL.getTabAt(1).setIcon(R.mipmap.whitelist);
                            tabL.getTabAt(2).setIcon(R.mipmap.cog);
                            //toolbar.setTitle("Your Request");
                            break;
                        default:
                            break;
                    }
                } else {
                    switch (tabL.getSelectedTabPosition()) {

                        case 0:
                            tabL.getTabAt(0).setIcon(R.mipmap.plus);
                            tabL.getTabAt(1).setIcon(R.mipmap.whitesong_schedule);
                            tabL.getTabAt(2).setIcon(R.mipmap.whitelist);
                            tabL.getTabAt(3).setIcon(R.mipmap.whitecog);
                            //do what you want when tab 0 is selected
                            break;
                        case 1:
                            tabL.getTabAt(0).setIcon(R.mipmap.whiteplus);
                            tabL.getTabAt(1).setIcon(R.mipmap.song_schedule);
                            tabL.getTabAt(2).setIcon(R.mipmap.whitelist);
                            tabL.getTabAt(3).setIcon(R.mipmap.whitecog);
                            //toolbar.setTitle("Pending Approval");
                            break;
                        case 2:
                            tabL.getTabAt(0).setIcon(R.mipmap.whiteplus);
                            tabL.getTabAt(1).setIcon(R.mipmap.whitesong_schedule);
                            tabL.getTabAt(2).setIcon(R.mipmap.list);
                            tabL.getTabAt(3).setIcon(R.mipmap.whitecog);
                            //toolbar.setTitle("Your Request");
                            break;
                        case 3:
                            tabL.getTabAt(0).setIcon(R.mipmap.whiteplus);
                            tabL.getTabAt(1).setIcon(R.mipmap.whitesong_schedule);
                            tabL.getTabAt(2).setIcon(R.mipmap.whitelist);
                            tabL.getTabAt(3).setIcon(R.mipmap.cog);
//                        tabL.getTabAt(4).setIcon(R.mipmap.whitecog);
                            //toolbar.setTitle("Tonight's List");
                            break;
                        default:
                            break;
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //viewPager.setCurrentItem(tab.getPosition());
                // Toast.makeText(LandingActivity.this, tab.getPosition(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}