package kdj.co.kdj_app.Model;

/**
 * Created by Vaibhav Barad on 12/5/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SongRequestModel {
    @SerializedName("result")
    @Expose
    private Long result;

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }

}
