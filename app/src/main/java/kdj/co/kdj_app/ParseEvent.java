package kdj.co.kdj_app;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Belal on 9/22/2015.
 */
public class ParseEvent {
    public static String[] ids;
    public static String[] names;
    public static String[] emails;
    public static String[] user;
    public static String[] date;

    public static final String JSON_ARRAY = "result";
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "kdj";
    public static final String KEY_EMAIL = "event";
    public static final String KEY_USER = "user_id";
    public static final String KEY_DATE = "date";
   // private JSONArray users = null;

    private String json;

    public ParseEvent (String json){
        this.json = json;
    }

    protected void parseJSON(){
        JSONArray jsonObject=null;
        try {
            jsonObject = new JSONArray(json);
            //users = jsonObject.getJSONArray();

            ids = new String[jsonObject.length()];
            names = new String[jsonObject.length()];
            emails = new String[jsonObject.length()];
            user = new String[jsonObject.length()];
            date = new String[jsonObject.length()];

            for(int i=0;i<jsonObject.length();i++){
                JSONObject jo = jsonObject.getJSONObject(i);
                ids[i] = jo.getString(KEY_ID);
                names[i] = jo.getString(KEY_NAME);
                emails[i] = jo.getString(KEY_EMAIL);
                user[i] = jo.getString(KEY_USER);
                date[i] = jo.getString(KEY_DATE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}