package kdj.co.kdj_app;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by asd on 26/07/2017.
 */

public class MyCustomApplication extends Application {
    private Bundle bundle;
    public static String eventID = "";
    public static String eventSong = "";
    private String strName,evtID;
    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
    }



    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

}
