package kdj.co.kdj_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import kdj.co.kdj_app.Activity.Events;
import kdj.co.kdj_app.Activity.Registration;
import kdj.co.kdj_app.Model.LoginModel;
import kdj.co.kdj_app.Retrofit.RetrofitApiUtils;
import kdj.co.kdj_app.Retrofit.RetrofitInterface;

public class MainActivity extends AppCompatActivity {


    EditText Mobile;
    EditText pass;
    public static String pass1, mob1;
    RetrofitInterface retrofitInterface;
    private Disposable subscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView login = (TextView) findViewById(R.id.textView3);
        Mobile = (EditText) findViewById(R.id.editText);
        pass = (EditText) findViewById(R.id.editText3);
        Button login1 = (Button) findViewById(R.id.button2);
        TextView bottom1 = (TextView) findViewById(R.id.textView4);
        TextView register = (TextView) findViewById(R.id.textView6);

        Button req_btn = (Button) findViewById(R.id.request_btn);
        EditText req_song_text_1 = (EditText) findViewById(R.id.StaticSongView);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Medium.ttf");
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Hairline.ttf");
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "fonts/Lato-LightItalic.ttf");
        Typeface custom_font4 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        Typeface custom_font5 = Typeface.createFromAsset(getAssets(), "fonts/CupolaBold.ttf");
        Typeface custom_font6 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Black.ttf");


        Mobile.setTypeface(custom_font2);
        pass.setTypeface(custom_font2);
        bottom1.setTypeface(custom_font3);


    /* HARILINE */

        login1.setTypeface(custom_font);
        login.setTypeface(custom_font5);
//        req_btn.setTypeface(custom_font);
//        req_song_text_1.setTypeface(custom_font2);
//        req_song_text_2.setTypeface(custom_font2);
//        req_song_text_3.setTypeface(custom_font2);

        SharedPreferences prefs = getSharedPreferences("Login-Info", MODE_PRIVATE);

        String mob_pref = prefs.getString("mob", "");
        String pass_pref = prefs.getString("pass", "");
        boolean isLoggedIn = prefs.getBoolean("loginDone", false);


        if (mob_pref != "" && isLoggedIn) {
            pass1 = pass_pref;
            mob1 = mob_pref;
            Intent i = new Intent(MainActivity.this, Events.class);
            startActivity(i);
            finish();

        }
        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Registration.class);
                startActivity(i);
                // close this activity
                finish();
            }
        });

        login1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                pass1 = pass.getText().toString();
                mob1 = Mobile.getText().toString();
                api_call();
            }
        });
    }


    public void api_call() {
        if (retrofitInterface == null) {
            retrofitInterface = RetrofitApiUtils.getGeneralAPIService();
        }
        Observable<ArrayList<LoginModel>> request = (Observable<ArrayList<LoginModel>>) RetrofitApiUtils.getPreparedObservable(retrofitInterface.login(pass1, mob1));
        request.subscribe(new Observer<ArrayList<LoginModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                subscription = d;
            }

            @Override
            public void onNext(ArrayList<LoginModel> loginModels) {
                if (loginModels.get(0).getResult() == 1) {
                    SharedPreferences.Editor editor = getSharedPreferences("Login-Info", MODE_PRIVATE).edit();
                    editor.putString("name", loginModels.get(0).getName());
                    editor.putString("admin", loginModels.get(0).getAdmin());
                    editor.putString("pass", loginModels.get(0).getPass());
                    editor.putString("email", loginModels.get(0).getEmail());
                    editor.putString("mob", loginModels.get(0).getMob());
                    editor.putString("id", loginModels.get(0).getId());

                    editor.putBoolean("loginDone", true);
                    editor.commit();
                    Intent i = new Intent(MainActivity.this, Events.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(MainActivity.this, "Invalid Mobile No. or Password", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

}
