package com.squareup.picasso;

/**
 * Created by Vaibhav Barad on 11/30/2017.
 */

public class PicassoTools {

    public static void clearCache (Picasso p) {
        p.cache.clear();
    }
}